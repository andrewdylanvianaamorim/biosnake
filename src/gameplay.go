package main

import (
	"log"

	"github.com/veandco/go-sdl2/sdl"
)

func gameplay() bool {
	var tick uint32
	janela, err := sdl.CreateWindow("Bio-Snake",
		sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED,
		larguraDaJanela, alturaDaJanela, sdl.WINDOW_FULLSCREEN)

	if err != nil {
		log.Fatal("Erro ao criar janela")
	}

	defer janela.Destroy()

	renderizador, err := sdl.CreateRenderer(janela, -1, sdl.RENDERER_ACCELERATED)

	if err != nil {
		renderizador.Destroy()
	}

	fundo := criaNovoFundo(renderizador)
	defer fundo.destruir()
	cobra := criaSnake(renderizador)
	defer cobra.destruir()
	lixo := criaNovoLixo(renderizador)
	defer lixo.destruir()
	for cobra.morto == false {
		tick = sdl.GetTicks()
		for evento := sdl.PollEvent(); evento != nil; evento = sdl.PollEvent() {
			switch evento.(type) {
			case *sdl.QuitEvent:
				cobra.morto = true
			}
		}

		cobra.update()
		lixo.update(&cobra)

		renderizador.Clear()

		fundo.desenhar(renderizador)
		lixo.desenhar(renderizador)
		cobra.desenhar(renderizador)

		renderizador.Present()

		if 1000/fps > sdl.GetTicks()-tick {
			sdl.Delay(1000/fps - (sdl.GetTicks() - tick))
		}
	}
	return true
}
