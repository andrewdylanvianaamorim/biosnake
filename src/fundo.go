package main

import (
	"log"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type Fundo struct {
	textura *sdl.Texture
}

func criaNovoFundo(renderizador *sdl.Renderer) Fundo {
	img.Init(img.INIT_PNG)

	arquivoBruto, err := img.Load("../res/sprites/fundo/fundo.png")

	if err != nil {
		log.Fatal("Não foi possível carregar o fundo.png")
	}

	texturaFundo, err := renderizador.CreateTextureFromSurface(arquivoBruto)

	if err != nil {
		log.Fatal("Não foi possível criar textura a partir de fundo.png")
	}

	return Fundo{texturaFundo}
}

func (f Fundo) desenhar(renderizador *sdl.Renderer) {
	renderizador.Copy(f.textura,
		&sdl.Rect{0, 0, larguraDoFundo, alturaDoFundo},
		&sdl.Rect{0, 0, larguraDaJanela, alturaDaJanela})
}

func (f *Fundo) destruir() {
	f.textura.Destroy()
}
