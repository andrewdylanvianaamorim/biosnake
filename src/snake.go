package main

import (
	"github.com/veandco/go-sdl2/sdl"
)

type Snake struct {
	cabeca Cabeca
	corpo  Corpo
	morto  bool
}

func criaSnake(renderizador *sdl.Renderer) Snake {
	cabeca := criaNovaCabeça(renderizador)
	corpo := criaNovoCorpo(renderizador)
	corpo.adicionar(cabeca.posicao[0], cabeca.posicao[1])

	return Snake{cabeca, corpo, false}
}

func (s *Snake) update() {
	//verificando eventos do teclado
	teclado := sdl.GetKeyboardState()

	if teclado[sdl.SCANCODE_UP] == 1 && s.cabeca.direcao != 2 {

		s.cabeca.direcao = 1
	} else if teclado[sdl.SCANCODE_DOWN] == 1 && s.cabeca.direcao != 1 {

		s.cabeca.direcao = 2
	} else if teclado[sdl.SCANCODE_LEFT] == 1 && s.cabeca.direcao != 4 {

		s.cabeca.direcao = 3
	} else if teclado[sdl.SCANCODE_RIGHT] == 1 && s.cabeca.direcao != 3 {

		s.cabeca.direcao = 4
	}

	//movendo a cobra
	switch s.cabeca.direcao {
	case 1:
		s.cabeca.texturaAtual = 0
		s.cabeca.posicao[1] -= velocidadeDaCobra
	case 2:
		s.cabeca.texturaAtual = 1
		s.cabeca.posicao[1] += velocidadeDaCobra
	case 3:
		s.cabeca.texturaAtual = 2
		s.cabeca.posicao[0] -= velocidadeDaCobra
	case 4:
		s.cabeca.texturaAtual = 3
		s.cabeca.posicao[0] += velocidadeDaCobra
	}

	//limitando a cobra
	switch true {
	case s.cabeca.posicao[0] < xMinimoDaCabeca:
		s.morto = true
		s.cabeca.posicao[0] = xMinimoDaCabeca
	case s.cabeca.posicao[0] > xMaximoDaCabeca:
		s.morto = true
		s.cabeca.posicao[0] = xMaximoDaCabeca
	case s.cabeca.posicao[1] < yMinimoDaCabeca:
		s.morto = true
		s.cabeca.posicao[1] = yMinimoDaCabeca
	case s.cabeca.posicao[1] > yMaximoDaCabeca:
		s.morto = true
		s.cabeca.posicao[1] = yMaximoDaCabeca
	}

	//movendo a cobra
	posicaoAtual, posicaoAntiga := [2]float64{s.cabeca.posicao[0] + larguraDoCorpoAumentada/2, s.cabeca.posicao[1] + alturaDoCorpoAumentada/2},
		[2]float64{s.cabeca.posicao[0] + larguraDoCorpoAumentada/2, s.cabeca.posicao[1] + alturaDoCorpoAumentada/2}

	for c := 0; c <= len(s.corpo.posicaoes)-1; c++ {
		posicaoAntiga = s.corpo.posicaoes[c]
		s.corpo.posicaoes[c] = posicaoAtual
		posicaoAtual = posicaoAntiga
	}

	//verificando a colisão da cabeça com o corpo

	if len(s.corpo.posicaoes) >= 5 {
		for c := 4; c <= len(s.corpo.posicaoes)-1; c++ {
			if (s.corpo.posicaoes[c][0] >= s.cabeca.posicao[0] && s.corpo.posicaoes[c][0] <= s.cabeca.posicao[0]+64) &&
				(s.corpo.posicaoes[c][1] >= s.cabeca.posicao[1] && s.corpo.posicaoes[c][1] <= s.cabeca.posicao[1]+64) {
				s.morto = true
			}
		}
	}

	//testando a colisão da cabeça
	//fmt.Println(s.morto)

	//s.morto = false
}
func (s Snake) desenhar(renderizador *sdl.Renderer) {
	for _, elemento := range s.corpo.posicaoes {
		renderizador.Copy(s.corpo.textura,
			&sdl.Rect{0, 0, larguraDoCorpo, alturaDoCorpo},
			&sdl.Rect{int32(elemento[0]), int32(elemento[1]), larguraDoCorpoAumentada, alturaDoCorpoAumentada})
	}

	renderizador.Copy(s.cabeca.textura,
		&sdl.Rect{s.cabeca.folhaDeSprite[s.cabeca.texturaAtual][0], s.cabeca.folhaDeSprite[s.cabeca.texturaAtual][1], larguraDaCabeca, alturaDaCabeca},
		&sdl.Rect{int32(s.cabeca.posicao[0]), int32(s.cabeca.posicao[1]), larguraDaCabecaAumentada, alturaDaCabecaAumentada})

}

func (s *Snake) destruir() {
	s.cabeca.textura.Destroy()
	s.corpo.textura.Destroy()
}
