package main

import (
	"log"
	"math/rand"
	"time"

	"github.com/veandco/go-sdl2/img"

	"github.com/veandco/go-sdl2/sdl"
)

type Lixo struct {
	posicao          [2]float64
	textura          *sdl.Texture
	folhaDeSprite    [4][2]int32
	texturaAtual     int
	geradorDeNumeros *rand.Rand
}

func criaNovoLixo(renderizador *sdl.Renderer) Lixo {
	img.Init(img.INIT_PNG)

	arquivoBruto, err := img.Load("../res/sprites/lixos/lixos.png")

	if err != nil {
		log.Fatal("não pode carregar lixos.png")
	}
	defer arquivoBruto.Free()

	textura, err := renderizador.CreateTextureFromSurface(arquivoBruto)

	if err != nil {
		log.Fatal("Não pode criar textura apartir de lixos.png")
	}

	folhaDeSprite := [4][2]int32{
		{0, 0},
		{64, 0},
		{0, 64},
		{64, 64},
	}

	geradorDeNumeros := rand.New(rand.NewSource(time.Now().UnixNano()))

	texturaAtual := int(geradorDeNumeros.Int31n(4))

	posicao := [2]float64{float64(geradorDeNumeros.Int31n(97)*10 + larguraDoLixo/2), float64(geradorDeNumeros.Int31n(41)*10 + alturaDoLixo/2)}

	return Lixo{posicao, textura, folhaDeSprite, texturaAtual, geradorDeNumeros}
}

func (l *Lixo) update(s *Snake) {
	if (l.posicao[0] >= s.cabeca.posicao[0]-200 && l.posicao[0] <= s.cabeca.posicao[0]+200) &&
		(l.posicao[1] >= s.cabeca.posicao[1]-200 && l.posicao[1] <= s.cabeca.posicao[1]+200) {
		switch true {
		case s.cabeca.direcao == 1:
			s.cabeca.texturaAtual = 4
		case s.cabeca.direcao == 2:
			s.cabeca.texturaAtual = 5
		case s.cabeca.direcao == 3:
			s.cabeca.texturaAtual = 6
		case s.cabeca.direcao == 4:
			s.cabeca.texturaAtual = 7
		}

		if (l.posicao[0] >= s.cabeca.posicao[0]-64 && l.posicao[0] <= s.cabeca.posicao[0]+64) &&
			(l.posicao[1] >= s.cabeca.posicao[1]-64 && l.posicao[1] <= s.cabeca.posicao[1]+64) {
			posicao := [2]float64{float64(l.geradorDeNumeros.Int31n(97)*10 + larguraDoLixo/2), float64(l.geradorDeNumeros.Int31n(41)*10 + alturaDoLixo/2)}
			l.texturaAtual = int(l.geradorDeNumeros.Int31n(4))
			l.posicao = posicao
			s.corpo.adicionar(s.corpo.posicaoes[len(s.corpo.posicaoes)-1][0], s.corpo.posicaoes[len(s.corpo.posicaoes)-1][1])
		}
	}

}

func (l Lixo) desenhar(renderizador *sdl.Renderer) {
	renderizador.Copy(l.textura,
		&sdl.Rect{l.folhaDeSprite[l.texturaAtual][0], l.folhaDeSprite[l.texturaAtual][1], larguraDoLixo, alturaDoLixo},
		&sdl.Rect{int32(l.posicao[0] + larguraDoLixo/2), int32(l.posicao[1] + alturaDoLixo/2), larguraDoLixo, alturaDoLixo},
	)
}

func (l *Lixo) destruir() {
	l.textura.Destroy()
}
