package main

import (
	"log"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type FundoMenu struct {
	textura *sdl.Texture
}

func criaNovoFundoMenu(renderizador *sdl.Renderer) FundoMenu {
	img.Init(img.INIT_PNG)

	arquivoBruto, err := img.Load("../res/ui/fundo/fundo.png")

	if err != nil {
		log.Fatal("Não foi possível carregar o fundo.png")
	}

	texturaFundo, err := renderizador.CreateTextureFromSurface(arquivoBruto)

	if err != nil {
		log.Fatal("Não foi possível criar textura a partir de fundo.png")
	}

	return FundoMenu{texturaFundo}
}

func (f FundoMenu) desenhar(renderizador *sdl.Renderer) {
	renderizador.Copy(f.textura,
		&sdl.Rect{0, 0, larguraDoFundo, alturaDoFundo},
		&sdl.Rect{0, 0, larguraDaJanela, alturaDaJanela})
}

func (f *FundoMenu) destruir() {
	f.textura.Destroy()
}
