package main

import (
	"log"

	"github.com/veandco/go-sdl2/sdl"
)

func main() {
	continuar := true
	janela, err := sdl.CreateWindow("Bio-Snake",
		sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED,
		larguraDaJanela, alturaDaJanela, sdl.WINDOW_FULLSCREEN)

	if err != nil {
		log.Fatal("Erro ao criar janela")
	}

	defer janela.Destroy()

	renderizador, err := sdl.CreateRenderer(janela, -1, sdl.RENDERER_ACCELERATED)

	if err != nil {
		renderizador.Destroy()
	}

	butaoJogar := criaNovoButao(renderizador, gameplay, [2]float64{576, 296}, [2][2]int32{
		{0, 0},
		{32, 0}})
	defer butaoJogar.destruir()

	butaoSair := criaNovoButao(renderizador, func() bool {
		return false
	}, [2]float64{1176, 620}, [2][2]int32{
		{0, 64},
		{32, 64}})
	defer butaoSair.destruir()

	fundoMenu := criaNovoFundoMenu(renderizador)
	defer fundoMenu.destruir()

	for continuar == true {
		for evento := sdl.PollEvent(); evento != nil; evento = sdl.PollEvent() {
			switch evento.(type) {
			case *sdl.QuitEvent:
				continuar = false
			}
		}
		butaoJogar.update()

		continuar = butaoSair.update()

		renderizador.Clear()

		fundoMenu.desenhar(renderizador)

		butaoJogar.desenhar(renderizador)

		butaoSair.desenhar(renderizador)

		renderizador.Present()
	}

}
