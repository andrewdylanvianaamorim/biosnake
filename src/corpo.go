package main

import (
	"log"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type Corpo struct {
	textura   *sdl.Texture
	posicaoes [][2]float64
}

func criaNovoCorpo(renderizador *sdl.Renderer) Corpo {
	img.Init(img.INIT_PNG)

	arquivoBruto, err := img.Load("../res/sprites/corpo/corpo.png")

	if err != nil {
		log.Fatal("Não pode carregar corpo.png")
	}

	defer arquivoBruto.Free()

	textura, err := renderizador.CreateTextureFromSurface(arquivoBruto)

	if err != nil {
		log.Fatal("Não pode criar textura apartir de corpo.png")
	}

	x := [][2]float64{}

	return Corpo{textura, x}
}

func (c *Corpo) adicionar(x, y float64) {
	c.posicaoes = append(c.posicaoes, [2]float64{x + larguraDoCorpoAumentada/2, y + alturaDoCorpoAumentada/2})
}
