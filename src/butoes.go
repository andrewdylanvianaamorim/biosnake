package main

import (
	"log"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type Butao struct {
	textura       *sdl.Texture
	clickado      func() bool
	posicao       [2]float64
	folhaDeSprite [2][2]int32
	texturaAtual  int // 0 - não pressionado, 1 - pressionado
}

func criaNovoButao(renderizador *sdl.Renderer, clickado func() bool, posicao [2]float64, folhaDeSprite [2][2]int32) Butao {
	img.Init(img.INIT_PNG)

	arquivoBruto, err := img.Load("../res/ui/botoes/botoes.png")

	if err != nil {
		log.Fatal("Não foi possível carregar botoes.png")
	}

	defer arquivoBruto.Free()

	textura, err := renderizador.CreateTextureFromSurface(arquivoBruto)

	if err != nil {
		log.Fatal("Não foi possível criar textura apartir de botoes.png")
	}

	return Butao{textura, clickado, posicao, folhaDeSprite, 0}

}

func (b Butao) desenhar(renderizador *sdl.Renderer) {
	renderizador.Copy(b.textura,
		&sdl.Rect{b.folhaDeSprite[b.texturaAtual][0], b.folhaDeSprite[b.texturaAtual][1], larguraDoBotao, alturaDoBotao},
		&sdl.Rect{int32(b.posicao[0]), int32(b.posicao[1]), larguraDoBotao * 4, alturaDoBotao * 4})
}

func (b *Butao) update() bool {
	var r bool
	x, y, estado := sdl.GetMouseState()
	if (float64(x) >= b.posicao[0] && float64(x) <= b.posicao[0]+128) &&
		(float64(y) >= b.posicao[1] && float64(y) <= b.posicao[1]+128) &&
		(estado == 1) {
		b.texturaAtual = 1
		b.clickado()
		r = false
	} else {
		r = true
	}
	b.texturaAtual = 0
	return r
}

func (b *Butao) destruir() {
	b.textura.Destroy()
}
