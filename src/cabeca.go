package main

import (
	"log"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type Cabeca struct {
	posicao       [2]float64
	direcao       int // 0 - parado, 1 - cima, 2 - baixo, 3 - esquerda, 4 - direita
	textura       *sdl.Texture
	folhaDeSprite [8][2]int32 // 0 - cima , 1 - baixo, 2 - esquerda, 3 - direita  - 4 - cima aberto - 5 baixo aberto - 6 esquerda aberto - 7 direita aberto
	texturaAtual  int
}

func criaNovaCabeça(renderizador *sdl.Renderer) Cabeca {

	img.Init(img.INIT_PNG)

	arquivoBruto, err := img.Load("../res/sprites/cobra/cobra.png")

	if err != nil {
		log.Fatal("Não foi possível carregar cobra.png")
	}

	texturaDaCabeca, err := renderizador.CreateTextureFromSurface(arquivoBruto)

	if err != nil {
		log.Fatal("Não foi possível foi possível criar textura apartir de cobra.png")
	}

	folhaDeSprite := [8][2]int32{
		{0, 96},
		{64, 64},
		{0, 64},
		{64, 32},
		{0, 32},
		{32, 64},
		{0, 0},
		{32, 0}}

	return Cabeca{[2]float64{50 + larguraDaCabecaAumentada/2, 50 + alturaDaCabecaAumentada/2}, 0, texturaDaCabeca, folhaDeSprite, 3}
}
