package main

const (
	larguraDoBotao           = 32
	alturaDoBotao            = 32
	larguraDaJanela          = 1280
	alturaDaJanela           = 720
	velocidadeDaCobra        = 32
	fps                      = 24
	larguraDoFundo           = 250
	alturaDoFundo            = 141
	larguraDaCabeca          = 32
	alturaDaCabeca           = 32
	larguraDaCabecaAumentada = 128
	alturaDaCabecaAumentada  = 128
	larguraDoCorpo           = 32
	alturaDoCorpo            = 32
	larguraDoCorpoAumentada  = 64
	alturaDoCorpoAumentada   = 64
	larguraDoLixo            = 64
	alturaDoLixo             = 64
	xMinimoDaCabeca          = -17
	xMaximoDaCabeca          = 1169
	yMinimoDaCabeca          = -12
	yMaximoDaCabeca          = 605
)
